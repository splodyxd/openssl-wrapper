/*
  <!-- Globals
*/

var exec = require('child_process').exec;
var fs = require('fs');
var uuid = require('node-uuid');
var validator = require('validator');

/*
  -->
  <!-- Local settings
*/

var binary = '/usr/bin/openssl';
var cipher = 'aes-256-cbc';
var tmpFolder = '/dev/shm/';

/*
  -->
  <!-- Exports
*/

exports.createPrivateKey = function(keylength, password, callback) {
  exec(binary + ' genrsa ' + keylength + ' -des3 -passout pass:' + password, function(err, stdout, stderr) {
    if (err) {
      return callback(new Error(err));
    } else {
      var privateKey;
      privateKey = stdout.substring(0, stdout.lastIndexOf("\n"));
      
      callback(null, privateKey);
    }
  });
}

exports.createPublicKey = function(privateKey, password, callback) {
  var tmpFile = tmpFolder + uuid.v4();

  fs.writeFile(tmpFile, privateKey, function(err){
    if (err) {
      return callback(new Error(err));
    } else {
      exec(binary + ' rsa -pubout -in ' + tmpFile + ' -passin pass:' + password, function(err, stdout, stderr) {
        if (err) {
          return callback(new Error(err));
        } else {
          var publicKey;
          publicKey = stdout.substring(0, stdout.lastIndexOf("\n"));
          
          callback(null, publicKey);
          
          fs.unlink(tmpFile, function (err) {
            if (err) throw err;
          });
        }
      });
    }
  });  
}

exports.encryptText = function(decText, password, base64, callback) {
  var tmpFile = tmpFolder + uuid.v4();
  
  if (base64 == true) {
    var base = '-base64';
  } else {
    var base = '';
  }
 
  fs.writeFile(tmpFile, decText, function(err){
    if (err) {
      return callback(new Error(err));
    } else {
      var cmdstring = binary + ' enc -' + cipher + ' -salt ' + base + ' -in ' + tmpFile + ' -pass pass:' + password;

      exec(cmdstring, function(err, stdout, stderr) {
        if (err) {
          return callback(new Error(err));
        } else {
          fs.writeFile(tmpFile + '.out', stdout, function(err) {
            if (err) {
              return callback(new Error(err));
            } else {
              var encText;
              encText = stdout.substring(0, stdout.lastIndexOf("\n"));

              callback(null, encText);

              fs.unlink(tmpFile, function (err) { if (err) throw err; });
              fs.unlink(tmpFile + '.out', function (err) { if (err) throw err; });
            }
          });
        }
      });
    }
  });
}

exports.decryptText = function(encText, password, base64, callback) {
  var tmpFile = tmpFolder + uuid.v4();

  if (base64 == true) {
    var base = '-base64';
  } else {
    var base = '';
  }

  fs.writeFile(tmpFile, encText + "\n", function(err){
    if (err) {
      return callback(new Error(err));
    } else {
      var cmdstring = binary + ' enc -d -' + cipher + ' -salt ' + base + ' -in ' + tmpFile + ' -pass pass:' + password;

      exec(cmdstring, function(err, stdout, stderr) {
        if (err) {
          return callback(new Error(err));
        } else {
          callback(null, stdout);

          fs.unlink(tmpFile, function (err) { if (err) throw err; });
        }
      })
    }
  });
}

exports.randomString = function(length, base64, callback) {
  if (base64 == true) {
    var base = '-base64';
  } else {
    var base = '';
  }
  
  if (!length) {
    var length = 128;
  }
  
  exec(binary + ' rand ' + base + ' ' + length, function (err, stdout, stderr) {
    if (err) {
      return callback(new Error(error));
    } else {
      var rand;
      rand = stdout.substring(0, stdout.lastIndexOf("\n"));
      
      callback(null, rand);
    }
  });
}

exports.encryptMessage = function(publicKey, message, base64, callback) {
  if (base64 == true) {
    var base = '| base64';
  } else {
    var base = '';
  }

  var tmpKey = tmpFolder + uuid.v4();
  var tmpFile = tmpFolder + uuid.v4();

  fs.writeFile(tmpKey, publicKey, function(err) {
    if(err) {
      return callback(new Error(err));
    } else {
      fs.writeFile(tmpFile, message, function(err) {
        if(err) {
          return callback(new Error(err));
        } else {
          var cmdstring = binary + ' rsautl -encrypt -pubin -inkey ' + tmpKey + ' -in ' + tmpFile + base;

          exec(cmdstring, function(err, stdout, stderr){
            if (err) {
              return callback(new Error(err));
            } else {
              encMsg = stdout.substring(0, stdout.lastIndexOf("\n"));

              callback(null, encMsg);
              
              fs.unlink(tmpFile, function (err) { if (err) throw err; });
              fs.unlink(tmpKey, function (err) { if (err) throw err; });
            }
          });
        }
      });
    }
  });
}

exports.decryptMessage = function(privateKey, encMsg, base64, callback) {
  if (base64 == true) {
    var base = 'base64 -d '
  } else {
    var base = '';
  }

  var tmpKey = tmpFolder + uuid.v4();
  var tmpFile = tmpFolder + uuid.v4();
  
  fs.writeFile(tmpKey, privateKey, function(err){
    if(err){
      return callback(new Error(err));
    } else {
      fs.writeFile(tmpFile, encMsg, function(err) {
        if(err){
          return callback(new Error(err));
        } else {
          var cmdstring = base + tmpFile + ' > ' + tmpFile + '.enc ; ' + binary + ' rsautl -decrypt -inkey ' + tmpKey + ' -in ' + tmpFile + '.enc';
          
          exec(cmdstring, function (err, stdout, stderr) {
            if (err) {
              return callback(new Error(err));
            } else {
              decMsg = stdout + "\n";
              
              callback(null, decMsg);
              
              fs.unlink(tmpFile, function (err) { if (err) throw err; });
              fs.unlink(tmpFile + '.enc', function (err) { if (err) throw err; });
              fs.unlink(tmpKey, function (err) { if (err) throw err; });
            }
          });
        }
      });
    }
  });
}
